require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  let(:valid_attributes) { {
      username: "kukkik",
      catagory: "find",
      title: "น้องมูมู่หาย",
      content: "น้องแมวลายส้ม ตัวเล็กๆ หายบริเวณฟิวเจอร์พาร์ครังสิต",
      image: "/public/images/mumu.jpg"
  } }

  let(:valid_session) { {
      username: "kukkik",
      password: "1234"

  } }

  describe "GET #index" do
    it "returns a success response" do
      Post.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Post" do
        expect {
          post :create, params: {post: valid_attributes}, session: valid_session
        }.to change(Post, :count).by(1)
      end

      it "redirects to the created post" do
        post :create, params: {post: valid_attributes}, session: valid_session
        expect(response).to redirect_to(root_url)
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { {
          username: "kukkik",
          catagory: "find",
          title: "น้องมีมี่หาย",
          content: "น้องแมวลายส้ม ตัวเไม่ล็ก หายบริเวณฟิวเจอร์พาร์ครังสิต",
          image: "/public/images/mumu.jpg"
      } }

      it "updates the requested post" do
        post = Post.create! valid_attributes
        put :update, params: {id: post.to_param, post: new_attributes}, session: valid_session
        post.reload
        new_attributes.each_pair do |key, value|
          expect(post[key]).to eq(value)
        end
      end

      it "redirects to the post" do
        post = Post.create! valid_attributes
        put :update, params: {id: post.to_param, post: valid_attributes}, session: valid_session
        expect(response).to redirect_to(root_url)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested post" do
      post = Post.create! valid_attributes
      expect {
        delete :destroy, params: {id: post.to_param}, session: valid_session
      }.to change(Post, :count).by(-1)
    end

    it "redirects to the posts list" do
      post = Post.create! valid_attributes
      delete :destroy, params: {id: post.to_param}, session: valid_session
      expect(response).to redirect_to(root_url)
    end
  end
end