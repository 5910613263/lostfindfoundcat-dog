require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:valid_attributes) {{
      username: "kukkik",
      password_digest: "1234",
      firstname: "suntara",
      lastname: "khangthanyakan",
      phone: "0877331768"
  }}

  let(:valid_session) { {
      username: "kukkik",
      password: "1234"

  } }

  describe "GET #index" do
    it "returns a success response" do
      User.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new User" do
        expect {
          post :create, params: {user: valid_attributes}
        }.to change(User, :count).by(0)
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {{
          username: "kukkik",
          password_digest: "1234",
          firstname: "suntara",
          lastname: "tuan",
          phone: "0877331768"
      }}

      it "updates the requested user" do
        user = User.create! valid_attributes
        put :update, params: {id: user.to_param, user: new_attributes}, session: valid_session
        user.reload
        new_attributes.each_pair do |key, value|
          expect(user[key]).to eq(value)
        end
      end

      it "redirects to the user" do
        user = User.create! valid_attributes
        put :update, params: {id: user.to_param, user: valid_attributes}, session: valid_session
        expect(response).to redirect_to(root_url)
      end
    end
  end

end