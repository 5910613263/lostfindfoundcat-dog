Feature: Manage users
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Creat new user
    Given I am on the new user page
    When I fill in user[username] with kukkik
    And I fill in user[password] with 1234
    And I fill in user[firstname] with Suntara
    And I fill in user[lastname] with Khangthanyakan
    And I fill in user[phone] with 0877331768
    And I press Create User
    Then I see the home page

  Scenario: Log in
    Given I have the user kukkik
    When I fill in username with kukkik
    And I fill in password with 1234
    And I press Login
    Then I should see the text Logged in!

  Scenario: Edit user
    Given kukkik is logged in
    And I am on the editing page
    When I fill in user[username] with kuk
    And I fill in user[password] with 1234
    And I fill in user[firstname] with Suntara
    And I fill in user[lastname] with Khangthanyakan
    And I fill in user[phone] with 0877331768
    And I press Update User
    Then I should see the text User was successfully updated.

  Scenario: Get post by user
    Given kukkik is logged in
    And I have the post
    When I press My Post
    Then I should see the text น้องมูมู่หาย

  Scenario: Log out
    Given kukkik is logged in
    When I press Log Out
    Then I should see the text Logged out!



#  Scenario: Delete user
#     Given the following users:
#      |name|surname|id_card|phone|username|password|
#      |name 1|surname 1|id_card 1|phone 1|username 1|password 1|
#      |name 2|surname 2|id_card 2|phone 2|username 2|password 2|
#      |name 3|surname 3|id_card 3|phone 3|username 3|password 3|
#      |name 4|surname 4|id_card 4|phone 4|username 4|password 4|
#    When I delete the 3rd user
#    Then I should see the fol lowing users:
#      |Name|Surname|Id card|Phone|Username|Password||||
#      |name 1|surname 1|id_card 1|phone 1|username 1|password 1|Show|Edit|Destroy|
#      |name 2|surname 2|id_card 2|phone 2|username 2|password 2|Show|Edit|Destroy|
#      |name 4|surname 4|id_card 4|phone 4|username 4|password 4|Show|Edit|Destroy|
