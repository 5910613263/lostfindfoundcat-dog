require 'uri'
require 'cgi'
require 'selenium-webdriver'
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "paths"))
require File.expand_path(File.join(File.dirname(__FILE__), "..", "support", "selectors"))

Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end

Given /^(?:|I )have the user (.+)$/ do |username|
  step "I am on #{"the new user page"}"
  step "I fill in #{"user[username]"} with #{username}"
  step "I fill in #{"user[password]"} with #{"1234"}"
  step "I fill in #{"user[firstname]"} with #{"Suntara"}"
  step "I fill in #{"user[lastname]"} with #{"Khangthanyakan"}"
  step "I fill in #{"user[phone]"} with #{"0877331768"}"
  step "I press #{"Create User"}"
  step "I see #{"the home page"}"
end

Given /^(?:|I )have the post/ do
  step "I am on #{"the new post page"}"
  step "I choose #{"post_catagory_lost"}"
  step "I fill in #{"post[title]"} with #{"น้องมูมู่หาย"}"
  step "I fill in #{"post[content]"} with #{"น้องแมวลายส้ม ตัวเล็กๆ หายบริเวณฟิวเจอร์พาร์ครังสิต"}"
  step "I attach a file"
  step "I press #{"Create Post"}"
  step "I should see the text #{"Post was successfully created."}"
end

Given /^(.*) is logged in$/ do |username|
  step "I have the user #{username}"
  step "I fill in #{"username"} with #{username}"
  step "I fill in #{"password"} with #{"1234"}"
  step "I press #{"Login"}"
  step "I should see the text #{"Logged in!"}"
end

When /^(?:|I )fill in (.+) with (.+)$/ do |field, value|
  fill_in(field, :with => value)
end

When /^(?:|I )press (.+)$/ do |button|
  click_link_or_button(button)
end

When /^(?:|I )cilck icon delete$/ do
  #within(".edit-delete") do
  find('.delete').click
  #end

  #page.accept_confirm { click_link "Delete" }
  #page.driver.browser.switch_to.alert.accept

  #page.evaluate_script('window.confirm = function() { return true; }')
  # page.click('Remove')
end

When /^(?:|I )choose (.+)$/ do |type|
  choose(type)
end

When(/^I attach a file$/) do
  attach_file('post_cover_picture', "#{::Rails.root}/public/images/mumu.jpg")
end

Then /^(?:|I )see (.+)$/ do |page_name|
  visit path_to(page_name)
end

Then /^(?:|I )should see the text (.+)$/ do |text|
  if page.respond_to? :should
    page.should has_text?(text)
  else
    assert page.has_text?(text)
  end
end
